from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.contrib.auth import get_user_model
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.contrib.auth import login, authenticate
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth import logout
from .token_generation import account_activation_token
from .forms import RegitstrationUserForm, LoginForm
from django.contrib.auth.models import Group
User = get_user_model()

def singup(request):
    form = RegitstrationUserForm(request.POST or None)
    if form.is_valid():
        user = form.save()
        user.set_password(user.password)
        group = Group.objects.get(name = "users")
        user.groups.add(group)
        user.save()
        domain=get_current_site(request).domain
        user.send_virification(domain)
        return render(request, 'users/confirmation_email.html', {})

    context = {
        'form' : form
    }
    return render(request, 'users/registration.html', context)

def activate_account(request, user_id, token):
    try:
        uid = user_id
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        return render(request, 'users/activated.html', {'description': 'Your account has been activate successfully'})
    else:
        return render(request, 'users/activated.html', {'description': 'Activation link is invalid!'})

def login_view(request):
    form = LoginForm(request.POST or None)
    if form.is_valid():
        email = form.cleaned_data['email']
        password = form.cleaned_data['password']
        login_user = authenticate(email= email, password=password)
        if login_user:
            login(request, login_user)
            return  HttpResponseRedirect(reverse('index'))
    context = {
        'form':form
    }
    return render(request, 'users/login.html', context)

def logout_view(request):
    logout(request)
    return  HttpResponseRedirect(reverse('index'))