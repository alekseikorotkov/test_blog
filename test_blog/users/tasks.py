from django.core.mail import EmailMessage
from time import sleep
from test_blog.celery import app

@app.task
def send_mail(subject, message, from_email, receiver):
    email =EmailMessage(subject=subject,body=message,from_email=from_email,to=[receiver])   
    email.send()
    return None
