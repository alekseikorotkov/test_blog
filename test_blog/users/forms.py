from django import forms 
from users.models import User

class RegitstrationUserForm(forms.ModelForm):
    date_of_birth = forms.DateField(widget=forms.DateInput(format = '%d/%m/%Y'), input_formats=('%d/%m/%Y',))
    password = forms.CharField(widget=forms.PasswordInput)
    class Meta:
        model = User
        fields = [
            'email',
            'first_name',
            'last_name',
            'date_of_birth',
            'password',
        ]
    def __init__(self, *args, **kwargs):
        super(RegitstrationUserForm, self).__init__(*args, **kwargs)
        self.fields['date_of_birth'].label = 'Date of birth dd/mm/yyyy'

class LoginForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField(widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)

    