from django.db import models
import uuid
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
# Create your models here.
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
import datetime
from users.managers import UserManager
from .token_generation import account_activation_token
from .tasks import send_mail

class User(AbstractBaseUser, PermissionsMixin):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.EmailField(('email address'), unique=True)
    first_name = models.CharField(('first name'), max_length=30)
    last_name = models.CharField(('last name'), max_length=30)
    date_of_birth = models.DateField()
    is_active = models.BooleanField(('active'), default=False)
    is_staff = models.BooleanField(('staff'), default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    date_of_birth
    REQUIRED_FIELDS = ['date_of_birth']
    # REQUIRED_FIELDS = []

    def send_notification(self, domain, id_post):
        email_subject = "New Comment!!!"

        message = render_to_string('users/notification_new_comment.html', {
                'name': self.first_name,
                'domain': domain,
                'pk': id_post,
                
            })
        send_mail.delay(subject=email_subject,message=message,from_email='kifegi9756@hideemail.net', receiver=self.email)

    def send_virification(self, domain):
        email_subject = 'Activate Your Account'
        message = render_to_string('users/activate_account.html', {
                'name': self.first_name,
                'domain': domain,
                'uuid': self.id,
                'token': account_activation_token.make_token(self),
            })
        send_mail.delay(subject=email_subject,message=message,from_email='kifegi9756@hideemail.net', receiver=self.email)