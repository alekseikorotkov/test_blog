from django.urls import path
from . import views as user_views 

urlpatterns = [
    path('singup/', user_views.singup , name='singup'),
    path('activate/<uuid:user_id>/<str:token>',user_views.activate_account, name='activate'),
    path('login/', user_views.login_view, name='login'),
    path('logout/', user_views.logout_view, name='logout'),
]