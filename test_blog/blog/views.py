from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.contrib.auth import get_user_model
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.contrib.auth import login, authenticate
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth import logout
from users.token_generation import account_activation_token
from users.forms import RegitstrationUserForm, LoginForm
User = get_user_model()
from posts.models import Post


# Create your views here.
def index(request):
    posts = Post.objects.filter(is_approve=True)
    context = {
        'posts': posts
    }
    return render(request,'blog/index.html', context)




