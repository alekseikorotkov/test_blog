from django import forms 
from .models import Post
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget


class EditPostForm(forms.ModelForm):
    # description = forms.Textarea(widget=SummernoteInplaceWidget(attrs={'maxlength':'4000'}), required=False)
    class Meta:
        model = Post
        fields = ['title','description']
        # widgets = {
        #     'foo': SummernoteWidget(),
        #     'bar': SummernoteInplaceWidget(),
        # }
        widgets = {
            'description': SummernoteWidget(),
        }
    def __init__(self, *args, **kwargs):
        super(EditPostForm, self).__init__(*args, **kwargs)