from django.contrib import admin

# Register your models here.
from posts.models import Post

from django_summernote.admin import SummernoteModelAdmin
from .models import Post

class PostAdmin(SummernoteModelAdmin):
    summernote_fields = ('description')
# Register your models here.
admin.site.register(Post, PostAdmin)