from django.db import models
from django.contrib.auth import get_user_model

# Create your models here.
User = get_user_model()

class Post(models.Model):
    title =  models.CharField(max_length=50)
    description = models.TextField()
    autor = models.ForeignKey(User,on_delete=models.CASCADE)
    date_create = models.DateTimeField(auto_now_add=True)
    is_approve = models.BooleanField(default=False)

    def __str__(self):
        return 'title: {} autor:{} date_create:{} is_approve:{}'.format(self.title, self.autor, self.date_create ,self.is_approve)