from django.shortcuts import render, reverse

from django.http import HttpResponseRedirect
from .forms import EditPostForm
from .models import Post
from posts.comment.forms import EditCommentForm
from posts.comment.models import Comment
# Create your views here.

def edit_post_view(request):
    form  = EditPostForm(request.POST or None)
    if form.is_valid():
    
        post = form.save(commit=False)
        post.autor = request.user
        if request.user.groups.filter(name = 'users') and len(request.user.groups.all()) == 1:
            post.is_approve = False
        else:
            post.is_approve = True
        post.save()
        
        return  HttpResponseRedirect(reverse('index'))
    context = {
        'form':form
    }
    return render(request, 'posts/edit_post.html', context)

def show_post_view(request, pk):
    post = Post.objects.get(id=pk)
    comments = Comment.objects.filter(post = post)
    comment_form = EditCommentForm()
    context = {
        
        'post':post,
        'comments':comments,
        'comment_form': comment_form,
    }
    return render(request, 'posts/show_post.html', context)
    