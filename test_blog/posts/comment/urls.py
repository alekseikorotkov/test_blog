from django.urls import path, include
from posts.comment import views as comment_views 

urlpatterns = [
    path('<int:pk>', comment_views.create_comment_view, name='add_comment')
    
]