from django.db import models
from posts.models import Post
from users.models import User
# Create your models here.
class Comment(models.Model):
    text = models.TextField()
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    autor = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    
    def __str__(self):
        return '{} {}'.format(self.autor, self.text)