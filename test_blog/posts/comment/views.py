from django.shortcuts import render,reverse
from posts.comment.models import Comment
from posts.models import Post
from posts.comment.forms import EditCommentForm
from django.http import HttpResponseRedirect
from django.contrib.sites.shortcuts import get_current_site

# Create your views here.
def create_comment_view(request,pk):
    form  = EditCommentForm(request.POST or None)
    if form.is_valid():
        
        comment = form.save(commit=False)
        comment.autor = request.user
        post = Post.objects.get(id=pk)
        domain=get_current_site(request).domain
        post.autor.send_notification(domain=domain,id_post=post.id)
        comment.post = post
        comment.save()
        
        return  HttpResponseRedirect(reverse('show_post', kwargs={'pk': comment.post.id}))
    context = {
        'form':form
    }
    return render(request, 'posts/edit_post.html', context)