from django import forms 
from posts.comment.models import Comment


class EditCommentForm(forms.ModelForm):
    
    class Meta:
        model = Comment
        fields = ['text']
        
    def __init__(self, *args, **kwargs):
        super(EditCommentForm, self).__init__(*args, **kwargs)