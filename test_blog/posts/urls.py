
from django.urls import path, include
from . import views as posts_views

urlpatterns = [
    
    path('', posts_views.edit_post_view, name='edit_post'),
    path('<int:pk>', posts_views.show_post_view, name='show_post'),
    path('comment/', include('posts.comment.urls')),
    
]