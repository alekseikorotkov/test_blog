# About project

Simple blog with opportunities:
  - registrartion users
  - create post
  - moderation posts
  - WYSIWYG editer post
### Installation
1) Clone [project](https://bitbucket.org/alekseikorotkov/test_blog/src/master/)
2) install virtualenv 
```sh
$ pip install virtualenv
```
3) create virtualenv
```sh
$ virtualenv venv 
```
4) activate virtualenv
```sh
$ source venv/bin/activate
```
5) Install requirements
```sh
$ pip install -r requirements.txt
```
6) run migrate
```sh
$ ./manage.py migrate
```
6) run create groups' users
```sh
$ ./manage.py create_group
```
### Run Application

run redis server in terminal 
```sh
$ redis-server
```
```sh
$ ./manage.py runserver
```
run celery's worker 
```sh
$ celery -A test_blog worker -l info
```







